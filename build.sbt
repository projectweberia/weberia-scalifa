organization := "Project Weberia"

name := "Scalifa"

version := "1.0.0"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "apache-repo-releases" at "http://repository.apache.org/content/repositories/releases/",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= {

  val (akkaV, streamHttp) = ("2.3.11", "1.0-RC4")

  // remember, %% means scala-major-version, so %% "bigdata" below will 
  // result error since there is no bigdata_2.11 version. use % instead
  // see: http://www.scala-sbt.org/0.13/tutorial/Library-Dependencies.html

  Seq(
    "org.scala-lang.modules"  %%   "scala-parser-combinators" % "1.0.4",
    "org.specs2"              %%  "specs2-core"                   % "3.6.2" % "test"
  )

}

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")
